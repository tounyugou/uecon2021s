// Fill out your copyright notice in the Description page of Project Settings.


#include "CppGameMode.h"

ACppGameMode::ACppGameMode(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.DoNotCreateDefaultSubobject(TEXT("Sprite"))) {

    DefaultPawnClass = ADefaultPawn::StaticClass();
    PlayerControllerClass = APlayerController::StaticClass();
    PlayerStateClass = APlayerState::StaticClass();
    GameStateClass = AGameStateBase::StaticClass();
    HUDClass = AHUD::StaticClass();
    GameSessionClass = AGameSession::StaticClass();
    SpectatorClass = ASpectatorPawn::StaticClass();
    ReplaySpectatorPlayerControllerClass = APlayerController::StaticClass();
    ServerStatReplicatorClass = AServerStatReplicator::StaticClass();
}