// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE2021SGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE2021S_API AUE2021SGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
