// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/DefaultPawn.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/HUD.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/SpectatorPawn.h"
#include "CppGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UE2021S_API ACppGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ACppGameMode(const FObjectInitializer& ObjectInitializer);

};
