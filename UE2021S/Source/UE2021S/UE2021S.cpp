// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE2021S.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE2021S, "UE2021S" );
